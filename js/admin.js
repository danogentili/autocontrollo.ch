$(function() {

    $("#signupForm input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            $("#btnSubmit").attr("disabled", true);
            event.preventDefault();
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var username = $("input#username").val();
            var unhashedpassword = $("input#passtwo").val();
            var password = hex_sha512(unhashedpassword);
            var sid = $("input#sid").val();
            var response = grecaptcha.getResponse();
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "https://controllo.autocontrollo.ch/iscrizioni.php",
                type: "POST",
                data: {
                    name: name,
                    email: email,
                    username: username,
                    sid: sid,
                    password: password,
                    response: response
                },
                cache: false,
                success: function(data) {
                    if(data == "ok"){
                        // Enable button & show success message
                        $("#btnSubmit").attr("disabled", false);
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>L'iscrizione &egrave; stata inviata. </strong>");
                        $('#success > .alert-success')
                            .append('</div>');
                        //clear all fields
                        $('#signupForm').trigger("reset");
                        location.reload(true);
                    } else {
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-danger').append("<strong>Scusa " + firstName + ", sembra che ci sia stato un errore :(!");
                        $('#success > .alert-danger').append('</div>');
                        //clear all fields
                        $('#signupForm').trigger("reset");
                    }
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Scusa " + firstName + ", sembra che ci sia stato un errore :(!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#signupForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#name').focus(function() {
    $('#success').html('');
});


function admin(stmt, what, where, table)
{
    // Of course there are limitations php side :)))
    $.ajax({
            url: "https://controllo.autocontrollo.ch/admin.php",
            type: "POST",
            data: {
                stmt: stmt,
                what: what,
                where: where,
                table: table
            },
            cache: false,
            success: function(data) {
                // Enable button & show success message
                if(data == "ok"){
                    alert("OK");
                    location.reload(true);
                } else { 
                    alert("ERROR");
                    location.reload(true);

                }
            },
            error: function() {
                // Fail message
                alert("ERROR");
                location.reload(true);
            },
    })
};
function printiscr(id) {
    var daniilarray = ["corso", "email", "nome", "datadinascita", "luogodinascita", "residenza", "via", "cap", "n", "numero", "cf", "data"];
    var arrayLength = daniilarray.length;
    for (var i = 0; i < arrayLength; i++) {
        cur = daniilarray[i];
        getin = "#"+id+cur;
        getout = "#"+cur;
        getin = $(getin).text();
        getout = $(getout).eq(0).html();
        getfull = getout+getin;
        document.getElementById(cur).innerHTML = getfull;
    }
    var printContents = document.getElementById("regolamento").innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload(true);
}
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
